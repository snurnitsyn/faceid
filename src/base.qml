import QtQuick 2.6
import QtQuick.Controls 2.0
import QtQuick.Window 2.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Universal 2.3
import QtQuick.Controls.Styles 1.4


ApplicationWindow {
    screen: Qt.application.screens[0]
    Component.onCompleted: {
        setX(screen.virtualX + screen.width / 2 - width / 2);
        setY(screen.height / 2 - height / 2);
    }

    id: applicationWindow
    title: qsTr("FaceID")
    minimumWidth: 640
    minimumHeight: 480
    color: "#222222"
    visible: false

    Universal.theme: Universal.Dark
    Universal.accent: Universal.Green

    Rectangle {
        id: loader
        width: parent.width / 2
        height: parent.height / 2
        visible: true
        color: "transparent"
        anchors.centerIn: parent
        transformOrigin: Item.Center

        ColumnLayout {
            width: parent.width
            height: parent.height

            Image {
                Layout.alignment: Qt.AlignHCenter
                Layout.preferredWidth: parent.width / 2
                Layout.preferredHeight: parent.height / 2
                fillMode:Image.PreserveAspectFit
                clip:true
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
                source: "icons/52/logo.svg"
            }

            BusyIndicator {
                Layout.alignment: Qt.AlignHCenter
                Layout.preferredWidth: parent.width / 8
                Layout.preferredHeight: parent.height / 8
                id: busyIndicator
                clip: false
            }
        }
    }

    // flags: Qt.FramelessWindowHint | Qt.WindowMinimizeButtonHint | Qt.Window // Отключаем обрамление окна

//    header : Rectangle {
//            id: systemTitleBar
//            width: parent.width
//            height: 40
//
//            color: "transparent"
//
//            ToolBar {
//                    id: mainInfo
//                    anchors.left: parent.left
//                    anchors.leftMargin: 16
//                    height: 40
//
//                    background: Rectangle {
//                        color: "transparent"
//                    }
//
//                    RowLayout {
//                        anchors.leftMargin: 0
//                        anchors.right: parent.right
//                        anchors.bottom: parent.bottom
//                        anchors.left: parent.left
//                        anchors.top: parent.top
//                        anchors.rightMargin: 0
//                        anchors.topMargin: 0
//                        transformOrigin: Item.Center
//                        spacing: 0
//
//                        Image {
//                            Layout.preferredWidth: 18
//                            Layout.preferredHeight: 18
//                            fillMode:Image.PreserveAspectFit
//                            clip:true
//                            horizontalAlignment: Image.AlignHCenter
//                            verticalAlignment: Image.AlignVCenter
//                            source: "icons/52/logo.png"
//                        }
//
//                        Label {
//                            height: parent.height
//                            color: "#ffffff"
//                            text: "FaceID"
//                            font.bold: false
//                            rightPadding: 20
//                            leftPadding: 10
//                            font.weight: Font.Light
//                            font.pixelSize: 16
//                            font.family: "Segoe UI"
//                            elide: Label.ElideLeft
//                            horizontalAlignment: Qt.AlignHCenter
//                            verticalAlignment: Qt.AlignVCenter
//                            Layout.fillWidth: true
//                        }
//                    }
//            }
//
//            ToolBar {
//                    id: menu
//                    anchors.right: parent.right
//                    anchors.rightMargin: 0
//                    height: 40
//
//                    background: Rectangle {
//                        color: "transparent"
//                    }
//
//                    RowLayout {
//                        anchors.leftMargin: 0
//                        anchors.right: parent.right
//                        anchors.bottom: parent.bottom
//                        anchors.left: parent.left
//                        anchors.top: parent.top
//                        anchors.rightMargin: 0
//                        anchors.topMargin: 0
//                        transformOrigin: Item.Center
//                        spacing: 0
//
//                        ToolButton {
//                            Layout.alignment: Qt.AlignRight
//                            id: sysHide
//                            y: 0
//                            Layout.preferredWidth: 60
//                            Layout.preferredHeight: parent.height
//                            rightPadding: 16
//                            leftPadding: 16
//                            contentItem: RowLayout {
//                                spacing: 12
//                                Image {
//                                    Layout.alignment: Qt.AlignCenter
//                                    Layout.preferredWidth: 26
//                                    Layout.preferredHeight: 26
//                                    fillMode:Image.PreserveAspectFit
//                                    clip:true
//                                    horizontalAlignment: Image.AlignHCenter
//                                    verticalAlignment: Image.AlignVCenter
//                                    source: "icons/52/minus.png"
//                                }
//                            }
//
//                            palette {
//                                button: hovered ? "#aaaaaa" : "transparent"
//                            }
//
//                            onClicked: application.show_minimized()
//                        }
//
//                        ToolButton {
//                            Layout.alignment: Qt.AlignRight
//                            id: sysClose
//                            y: 0
//                            Layout.preferredWidth: 60
//                            Layout.preferredHeight: parent.height
//                            rightPadding: 16
//                            leftPadding: 16
//                            contentItem: RowLayout {
//                                spacing: 12
//                                Image {
//                                    Layout.alignment: Qt.AlignCenter
//                                    Layout.preferredWidth: 26
//                                    Layout.preferredHeight: 26
//                                    fillMode:Image.PreserveAspectFit
//                                    clip:true
//                                    horizontalAlignment: Image.AlignHCenter
//                                    verticalAlignment: Image.AlignVCenter
//                                    source: "icons/52/close.png"
//                                }
//                            }
//
//                            background: Rectangle {
//                                color: sysClose.hovered ? "#c40d1d" : "transparent"
//                            }
//
//                            onClicked: Qt.quit()
//                        }
//                    }
//            }
//    }

    Rectangle {
        id: menuBar
        width: parent.width
        height: 60
        visible: false

        color: "transparent"

        ToolBar {
                id: toolbarLeft
                anchors.left: parent.left
                anchors.leftMargin: 0
                height: 60

                background: Rectangle {
                    color: "transparent"
                }

                RowLayout {
                    anchors.leftMargin: 0
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.rightMargin: 0
                    anchors.topMargin: 0
                    transformOrigin: Item.Center
                    spacing: 0

                    ToolButton {
                        Layout.alignment: Qt.AlignLeft
                        id: menuButton
                        y: 0
                        Layout.preferredHeight: parent.height
                        rightPadding: 16
                        leftPadding: 16
                        clip: false
                        contentItem: RowLayout {
                            Image {
                                Layout.alignment: Qt.AlignCenter
                                source: "icons/52/menu.png"
                                Layout.preferredWidth: 26
                                Layout.preferredHeight: 26
                                fillMode:Image.PreserveAspectFit
                                clip:true
                                horizontalAlignment: Image.AlignHCenter
                                verticalAlignment: Image.AlignBottom
                            }
                        }

                        palette {
                            button: hovered ? "#aaaaaa" : "transparent"
                        }

                        onClicked: {
                            textEdit.visible = false
                            deleteButton.visible = false
                            recognitionButton.visible = false
                            identificationButton.visible = false

                            settingsSave.visible = true
                            menuButton.visible = false
                            settings.visible = true
                            workplace.visible = false
                        }
                    }

                    ToolButton {
                        visible: false
                        Layout.alignment: Qt.AlignLeft
                        id: settingsSave
                        y: 0
                        Layout.preferredHeight: parent.height
                        rightPadding: 16
                        leftPadding: 16
                        clip: false
                        contentItem: RowLayout {
                            Image {
                                Layout.alignment: Qt.AlignCenter
                                source: "icons/52/ok.png"
                                Layout.preferredWidth: 26
                                Layout.preferredHeight: 26
                                fillMode:Image.PreserveAspectFit
                                clip:true
                                horizontalAlignment: Image.AlignHCenter
                                verticalAlignment: Image.AlignBottom
                            }
                        }

                        palette {
                            button: hovered ? "#aaaaaa" : "transparent"
                        }

                        onClicked: {
                            settingsSave.visible = false
                            menuButton.visible = true
                            settings.visible = false
                            workplace.visible = true

                            textEdit.visible = true
                            deleteButton.visible = true
                            recognitionButton.visible = true
                            identificationButton.visible = true
                            var objects = JSON.stringify(
                                {
                                    "cam": settingsParamCamera.text,
                                    "rot": settingsParamRotation.text,
                                    "face_size": settingsParamFaceSize.text
                                }
                            )
                            application.save_config(objects)
                        }
                    }

                    TextField {
                        Layout.alignment: Qt.AlignLeft
                        id: textEdit
                        Layout.preferredWidth: 300
                        height: 26
                        horizontalAlignment: Text.AlignLeft
                        bottomPadding: 8
                        font.wordSpacing: 0
                        rightPadding: 21
                        font.pixelSize: 22
                        font.family: "Segoe UI"
                        selectedTextColor: "#aaaaaa"
                        color: "#ffffff"
                        placeholderText: qsTr("Введите имя")

                        background: Rectangle {
                            color: "transparent"
                        }
                    }
                }
        }

        ToolBar {
                id: toolbar
                anchors.right: parent.right
                anchors.rightMargin: 0
                height: 60

                background: Rectangle {
                    color: "transparent"
                }

                RowLayout {
                    anchors.leftMargin: 0
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.rightMargin: 0
                    anchors.topMargin: 0
                    transformOrigin: Item.Center
                    spacing: 0

                    ToolButton {
                        visible: false
                        Layout.alignment: Qt.AlignRight
                        id: identificationButtonStop
                        y: 0
                        Layout.preferredHeight: parent.height
                        rightPadding: 16
                        leftPadding: 16
                        contentItem: RowLayout {
                            Image {
                                id: identificationImageStop
                                Layout.alignment: Qt.AlignCenter
                                Layout.preferredWidth: 26
                                Layout.preferredHeight: 26
                                fillMode:Image.PreserveAspectFit
                                clip:true
                                horizontalAlignment: Image.AlignHCenter
                                verticalAlignment: Image.AlignVCenter
                                source: "icons/52/learn_ok.png"
                            }
                            Label {
                                id: identificationLabelStop
                                text: "Закончить обучение"
                                visible: applicationWindow.width < 800 ? false : true
                                font.pixelSize: 16
                                font.family: "Segoe UI"
                                color: "#2ecc71"
                                elide: Label.ElideRight
                                horizontalAlignment: Qt.AlignHCenter
                                verticalAlignment: Qt.AlignVCenter
                                Layout.fillWidth: true
                            }
                        }

                        palette {
                            button: hovered ? "#aaaaaa" : "transparent"
                        }

                        onClicked: {
                            deleteButton.enabled = false
                            deleteButton.opacity = 0.7
                            deleteImage.opacity = 0.7

                            recognitionButton.enabled = false
                            recognitionButton.opacity = 0.7
                            recognitionImage.opacity = 0.7

                            identificationButtonStop.enabled = false
                            identificationImageStop.opacity = 0.7
                            identificationButtonStop.opacity = 0.7

                            application.stop_identification()
                        }
                    }

                    ToolButton {
                        Layout.alignment: Qt.AlignRight
                        id: identificationButton
                        y: 0
                        Layout.preferredHeight: parent.height
                        rightPadding: 16
                        leftPadding: 16
                        contentItem: RowLayout {
                            Image {
                                Layout.alignment: Qt.AlignCenter
                                Layout.preferredWidth: 26
                                Layout.preferredHeight: 26
                                fillMode:Image.PreserveAspectFit
                                clip:true
                                horizontalAlignment: Image.AlignHCenter
                                verticalAlignment: Image.AlignVCenter
                                source: "icons/52/learn.png"
                            }
                            Label {
                                id: identificationLabel
                                text: "Обучение"
                                visible: applicationWindow.width < 800 ? false : true
                                font.pixelSize: 16
                                font.family: "Segoe UI"
                                color: "#ffffff"
                                elide: Label.ElideRight
                                horizontalAlignment: Qt.AlignHCenter
                                verticalAlignment: Qt.AlignVCenter
                                Layout.fillWidth: true
                            }
                        }

                        palette {
                            button: hovered ? "#aaaaaa" : "transparent"
                        }

                        onClicked: application.start_identification(textEdit.text)
                    }

                    ToolButton {
                        Layout.alignment: Qt.AlignRight
                        id: recognitionButton
                        y: 0
                        Layout.preferredHeight: parent.height
                        rightPadding: 16
                        leftPadding: 16
                        clip: false
                        font.pointSize: 13
                        contentItem: RowLayout {
                            Image {
                                id: recognitionImage
                                Layout.alignment: Qt.AlignCenter
                                source: "icons/52/search.png"
                                Layout.preferredWidth: 26
                                Layout.preferredHeight: 26
                                fillMode:Image.PreserveAspectFit
                                clip:true
                                horizontalAlignment: Image.AlignHCenter
                                verticalAlignment: Image.AlignVCenter
                            }

                            Label {
                                id: recognitionLabel
                                visible: applicationWindow.width < 800 ? false : true
                                text: "Разпознавание"
                                font.pixelSize: 16
                                font.family: "Segoe UI"
                                color: "#ffffff"
                                elide: Label.ElideRight
                                horizontalAlignment: Qt.AlignHCenter
                                verticalAlignment: Qt.AlignVCenter
                                Layout.fillWidth: true
                            }
                        }

                        palette {
                            button: hovered ? "#aaaaaa" : "transparent"
                        }

                        onClicked: {
                            deleteButton.enabled = true
                            recognitionButton.opacity = 1
                            deleteImage.opacity = 1

                            deleteButton.enabled = true
                            deleteButton.opacity = 1
                            deleteImage.opacity = 1
                            application.start_recognition(textEdit.text)
                        }
                    }

                    ToolButton {
                        Layout.alignment: Qt.AlignRight
                        id: deleteButton
                        y: 0
                        Layout.preferredHeight: parent.height
                        rightPadding: 16
                        leftPadding: 16
                        clip: false
                        font.pointSize: 13
                        contentItem: RowLayout {
                            Image {
                                Layout.alignment: Qt.AlignCenter
                                id: deleteImage
                                source: "icons/52/delete.png"
                                Layout.preferredWidth: 26
                                Layout.preferredHeight: 26
                                fillMode:Image.PreserveAspectFit
                                clip:true
                                horizontalAlignment: Image.AlignHCenter
                                verticalAlignment: Image.AlignVCenter
                            }

                            Label {
                                id: deleteLabel
                                visible: applicationWindow.width < 800 ? false : true
                                text: "Удаление"
                                font.pixelSize: 16
                                font.family: "Segoe UI"
                                color: "#ffffff"
                                elide: Label.ElideRight
                                horizontalAlignment: Qt.AlignHCenter
                                verticalAlignment: Qt.AlignVCenter
                                Layout.fillWidth: true
                            }
                        }

                        palette {
                            button: hovered ? "#aaaaaa" : "transparent"
                        }

                        onClicked: application.delete_user(textEdit.text)
                    }
                }
        }

    }

    Rectangle {
        visible: false
        id: workplace
        anchors.top: menuBar.bottom
        anchors.topMargin: 0
        height: parent.height - menuBar.height + 40
        width: parent.width

        color: "#191919"

        Label {
            id: cameraLabel
            width: parent.width
            height: parent.height

            Image {
                id: image
                anchors.rightMargin: 5
                anchors.leftMargin: 5
                anchors.bottomMargin: 5
                anchors.topMargin: 5
                anchors.fill: parent
                source: ""
                fillMode: Image.PreserveAspectFit
                cache: false
                function reload() {
                    source = "";
                    source = "image://myprovider/capture.png";
                }
            }
        }

    }

    Flickable   {
        anchors.fill: parent
        contentHeight: flow.implicitHeight + 40
        visible: false
        id: settings
        anchors.top: menuBar.bottom
        anchors.topMargin: 60
        height: parent.height - menuBar.height + 40
        width: parent.width

        anchors.right: parent.right
        anchors.left: parent.left
        clip: true

        Flow {
            id: flow
            width: parent.width
            anchors.top: parent.top
            anchors.topMargin: parent.height > childrenRect.height + 40 ? (parent.height - childrenRect.height)/2 : 20
            anchors.left: parent.left
            anchors.leftMargin: (parent.width - childrenRect.width - 40)/2
            leftPadding: 20
            rightPadding: 20
            spacing: 12

            Item {

                width: 200
                height: 200

                ToolButton {
                    id: settingParamCamera
                    anchors.fill: parent

                    background: Rectangle {
                        color: "#252525"
                        border.width: 1
                        border.color: "#606060"

                         TextField {
                            id: settingsParamCamera
                            width: parent.width
                            anchors.bottom: labTitleCamera.top
                            anchors.bottomMargin: 10
                            horizontalAlignment: Text.AlignHCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            bottomPadding: 8
                            font.wordSpacing: 0
                            font.pixelSize: 22
                            font.family: "Segoe UI"
                            selectedTextColor: "#29b8cc"
                            color: "#ffffff"
                            placeholderText: qsTr("NUMBER")

                            background: Rectangle {
                                color: "transparent"
                            }
                        }

                        Label {
                            id: labTitleCamera
                            font.bold: true
                            anchors.centerIn: parent
                            color: "#ffffff"
                            text: "Камера"
                        }

                        Label {
                            id: labDescriptionCamera
                            anchors.top: labTitleCamera.bottom
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width
                            wrapMode: Text.WordWrap
                            horizontalAlignment: Text.AlignHCenter
                            padding: 12
                            color: "#b5b3b3"
                            text: "Укажите идентификатор камеры"
                        }
                    }

                    opacity: pressed ? 0.9 : 1
                    scale: pressed ? 0.97 : 1
                }
            }

            Item {

                width: 200
                height: 200

                ToolButton {
                    id: settingParamRotation
                    anchors.fill: parent

                    background: Rectangle {
                        color: "#252525"
                        border.width: 1
                        border.color: "#606060"

                        TextField {
                            id: settingsParamRotation
                            width: parent.width
                            anchors.bottom: labTitleRotation.top
                            anchors.bottomMargin: 10
                            horizontalAlignment: Text.AlignHCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            bottomPadding: 8
                            font.wordSpacing: 0
                            font.pixelSize: 22
                            font.family: "Segoe UI"
                            selectedTextColor: "#29b8cc"
                            color: "#ffffff"
                            placeholderText: qsTr("NUMBER")

                            background: Rectangle {
                                color: "transparent"
                            }
                        }

                        Label {
                            id: labTitleRotation
                            font.bold: true
                            anchors.centerIn: parent
                            color: "#ffffff"
                            text: "Угол"
                        }

                        Label {
                            id: labDescriptionRotation
                            anchors.top: labTitleRotation.bottom
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width
                            wrapMode: Text.WordWrap
                            horizontalAlignment: Text.AlignHCenter
                            padding: 12
                            color: "#b5b3b3"
                            text: "Укажите угол поворота камеры"
                        }
                    }

                    opacity: pressed ? 0.9 : 1
                    scale: pressed ? 0.97 : 1
                }
            }

            Item {

                width: 200
                height: 200

                ToolButton {
                    id: settingParamFaceSize
                    anchors.fill: parent

                    background: Rectangle {
                        color: "#252525"
                        border.width: 1
                        border.color: "#606060"

                        TextField {
                            id: settingsParamFaceSize
                            width: parent.width
                            anchors.bottom: labTitleFaceSize.top
                            anchors.bottomMargin: 10
                            horizontalAlignment: Text.AlignHCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            bottomPadding: 8
                            font.wordSpacing: 0
                            font.pixelSize: 22
                            font.family: "Segoe UI"
                            selectedTextColor: "#29b8cc"
                            color: "#ffffff"
                            placeholderText: qsTr("NUMBER")

                            background: Rectangle {
                                color: "transparent"
                            }
                        }

                        Label {
                            id: labTitleFaceSize
                            font.bold: true
                            anchors.centerIn: parent
                            color: "#ffffff"
                            text: "Размер лица"
                        }

                        Label {
                            id: labDescriptionFaceSize
                            anchors.top: labTitleFaceSize.bottom
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: parent.width
                            wrapMode: Text.WordWrap
                            horizontalAlignment: Text.AlignHCenter
                            padding: 12
                            color: "#b5b3b3"
                            text: "Укажите минимальный размер лица в пикселях"
                        }
                    }

                    opacity: pressed ? 0.9 : 1
                    scale: pressed ? 0.97 : 1
                }
            }

        }

        ScrollBar.vertical: ScrollBar { }
    }

    function updateImage() {
        image.reload()
    }

    function finishLoading() {
        loader.visible = false
        menuBar.visible = true
        workplace.visible = true
    }

    function allowActionInFinishingIdentification() {
        deleteButton.enabled = true
        deleteButton.opacity = 1
        deleteImage.opacity = 1

        recognitionButton.enabled = true
        recognitionButton.opacity = 1
        recognitionImage.opacity = 1

        identificationButtonStop.enabled = true
        identificationImageStop.opacity = 1
        identificationButtonStop.opacity = 1
    }

    function startIdentification() {
        identificationButtonStop.visible = false
        deleteButton.enabled = false
        deleteButton.opacity = 0.7
        deleteImage.opacity = 0.7
    }

    function stopIdentification() {
        identificationButtonStop.visible = false
        identificationButton.visible = true

        identificationButtonStop.visible = false
    }

    function allowSuccessfulFinishgIdentification() {
        identificationButton.visible = false
        identificationButtonStop.visible = true
    }

    function setSettingsParameters(jsonString) {
        if (jsonString) {
            var jsonObj = JSON.parse(jsonString)
            if (jsonObj) {
                if (jsonObj.hasOwnProperty("cam")) {
                    settingsParamCamera.text = jsonObj["cam"]
                }
                if (jsonObj.hasOwnProperty("rot")) {
                    settingsParamRotation.text = jsonObj["rot"]
                }
                if (jsonObj.hasOwnProperty("face_size")) {
                    settingsParamFaceSize.text = jsonObj["face_size"]
                }
            }
        }
    }

}