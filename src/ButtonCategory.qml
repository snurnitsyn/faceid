import QtQuick 2.0
import QtQuick.Controls 2.2

Item {

    property alias title: labTitle.text
    property alias description: labDescription.text

    signal clicked

    width: 200
    height: 200

    ToolButton {
        id: settingParam
        anchors.fill: parent

        background: Rectangle {
            color: "#252525"
            border.width: 1
            border.color: "#606060"

             TextField {
                id: textField
                Layout.alignment: Qt.AlignRight
                width: 100
                height: 26
                horizontalAlignment: Text.AlignLeft
                bottomPadding: 8
                font.wordSpacing: 0
                rightPadding: 21
                font.pixelSize: 22
                font.family: "Segoe UI"
                selectedTextColor: "#29b8cc"
                color: "#aaaaaa"
                placeholderText: qsTr("NUMBER")

                background: Rectangle {
                    color: "transparent"
                }
            }
            TextEdit {
                id: pixmap
                y: 16
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Label {
                id: labTitle
                font.bold: true
                anchors.centerIn: parent
                color: "#ffffff"
            }

            Label {
                id: labDescription
                anchors.top: labTitle.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                padding: 12
                color: "#b5b3b3"
            }
        }

        opacity: pressed ? 0.9 : 1
        scale: pressed ? 0.97 : 1
    }
}