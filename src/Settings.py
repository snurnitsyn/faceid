import cv2
import json


class Settings:
    def __init__(self, args):
        super().__init__()
        self.faceSize = int(args.faceSizeMin) if args.cam.isdigit() else 80
        self.cam = cv2.CAP_DSHOW + int(args.cam) if args.cam.isdigit() else cv2.CAP_DSHOW
        self.rotation = int(args.rotation) if args.rotation.isdigit() else 0
        self.jsonpath = args.json
        self.vs = None
        self.set_cam(str(self.cam))

    def get_cam(self):
        return self.cam

    def get_face_size(self):
        return self.faceSize

    def get_rotation(self):
        return self.rotation

    def get_capture(self):
        return self.vs

    def get_json_path(self):
        return self.jsonpath

    def refresh_cam(self):
        if self.vs is not None and self.vs.isOpened():
            return self.vs

        cap = cv2.VideoCapture(cv2.CAP_DSHOW + int(self.cam))
        if cap is None or not cap.isOpened():
            print('Warning: unable to open video source: ', self.cam)
            cap.release()
            return None
        else:
            if self.vs is not None and self.vs.isOpened():
                self.vs.release()
            self.vs = cap
            return self.vs

    def set_cam(self, cam):
        if cam.isdigit() and cv2.CAP_DSHOW + int(cam) != self.cam:
            cap = cv2.VideoCapture(cv2.CAP_DSHOW + int(cam))
            if cap is None or not cap.isOpened():
                print('Warning: unable to open video source: ', cam)
                cap.release()
            else:
                if self.vs is not None and self.vs.isOpened():
                    self.vs.release()
                self.vs = cap

    def set_face_size(self, face_size):
        if face_size.isdigit():
            if int(face_size) > 20:
                self.faceSize = int(face_size)
            else:
                self.faceSize = 20

    def set_rotation(self, rotation):
        if rotation.isdigit():
            self.rotation = int(rotation)

    def update(self, dictionary):
        try:
            if dictionary is not None:
                if 'cam' in dictionary:
                    self.set_cam(dictionary['cam'])
                if 'rot' in dictionary:
                    self.set_rotation(dictionary['rot'])
                if 'face_size' in dictionary:
                    self.set_face_size(dictionary['face_size'])
        except Exception as e:
            print(str(e))

    def get_json_str(self):
        return json.dumps({
            'rot': self.rotation,
            'cam': self.cam - cv2.CAP_DSHOW,
            'face_size': self.faceSize
        })

