from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSignal, pyqtSlot

from src.FaceID import findPeople
import cv2
import numpy
import imutils


class Verification(QtCore.QThread):
    updateImage = pyqtSignal(numpy.ndarray)
    disableCam = pyqtSignal()

    def __init__(self, parent=None, face_detect=None, aligner=None, extract_feature=None, name=None, config=None):
        self.face_detect = face_detect
        self.aligner = aligner
        self.extract_feature = extract_feature
        self.targetName = name
        self.running = False
        self.config = config

        QtCore.QThread.__init__(self, parent)

    def run(self):
        try:
            self.running = True
            while self.running:
                _, frame = self.config.get_capture().read()
                frame = imutils.rotate_bound(frame, self.config.get_rotation())
                rects, landmarks = self.face_detect.detect_face(frame, self.config.get_face_size())

                aligns = []
                positions = []
                if len(rects) == 0:
                    self.updateImage.emit(frame)
                else:
                    for (i, rect) in enumerate(rects):
                        aligned_face, face_pos = self.aligner.align(160, frame, landmarks[i])
                        if len(aligned_face) == 160 and len(aligned_face[0]) == 160:
                            aligns.append(aligned_face)
                            positions.append(face_pos)
                        else:
                            print("Align face failed")
                    if len(aligns) > 0:
                        features_arr = self.extract_feature.get_features(aligns)
                        recog_data = findPeople(features_arr, self.config.get_json_path(), positions)
                        for (i, rect) in enumerate(rects):
                            color = (0, 255, 0)
                            text_color = (255, 255, 255)
                            font = cv2.FONT_HERSHEY_COMPLEX
                            thinkness = 1
                            name = recog_data[i][0]
                            percent = str("%.2f" % recog_data[i][1]) + "%"
                            if name == self.targetName:
                                color = (0, 215, 255)
                                thinkness = 1
                            if name == "":
                                thinkness = 1
                                color = (255, 255, 255)
                                percent = ""

                            cv2.rectangle(frame, (rect[0], rect[1]), (rect[0] + rect[2], rect[1] + rect[3]), color,
                                          thickness=thinkness)

                            cv2.putText(frame, name, (rect[0] + 5, rect[1] + rect[3] - 5),
                                        font, 0.7, text_color, 1, cv2.LINE_AA)

                            if name != "":
                                cv2.putText(frame, percent, (rect[0] + rect[2], rect[1] + rect[3]),
                                            font, 1, text_color, 1, cv2.LINE_AA)

                            self.updateImage.emit(frame)
                        self.updateImage.emit(frame)
                    self.updateImage.emit(frame)

                if not self.running:
                    self.quit()

        except Exception as e:
            self.disableCam.emit()
            self.interrupt()

    @pyqtSlot(name="interrupt")
    def interrupt(self):
        self.running = False
        print("Верификация прервана по просьбе пользователя")
        print("Verification thread is killed")
        self.wait()
