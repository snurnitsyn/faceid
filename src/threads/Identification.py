from PyQt5 import QtCore
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from scipy.ndimage.filters import gaussian_filter

import os
import json
import numpy as np
import imutils


class Identification(QtCore.QThread):
    updateImage = pyqtSignal(np.ndarray)
    disableCam = pyqtSignal()
    successfulSaveIdentificationResult = pyqtSignal(str)
    allowStopIdentification = pyqtSignal()

    def __init__(self, parent=None, face_detect=None, aligner=None, extract_feature=None, name=None, config=None):
        QtCore.QThread.__init__(self, parent)
        self.__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        self.name = name
        self.aligner = aligner
        self.extract_feature = extract_feature
        self.face_detect = face_detect
        self.config = config

        self.running = False
        self.success = False

        f = open(self.config.get_json_path(), 'r')
        self.data_set = json.loads(f.read())
        self.person_imgs = {"Left": [], "Right": [], "Center": []}
        self.person_features = {"Left": [], "Right": [], "Center": []}
        self.max_length = 50

    def run(self):
        try:
            self.running = True
            while self.running:
                _, frame = self.config.get_capture().read()
                rects, landmarks = self.face_detect.detect_face(frame, self.config.get_face_size())
                frame = imutils.rotate_bound(frame, self.config.get_rotation())
                for (i, rect) in enumerate(rects):
                    aligned_frame, pos = self.aligner.align(160, frame, landmarks[i])
                    if len(aligned_frame) == 160 and len(aligned_frame[0]) == 160:

                        if pos == "Left" and len(self.person_imgs["Left"]) <= self.max_length:
                            self.person_imgs[pos].append(aligned_frame)

                        if pos == "Right" and len(self.person_imgs["Right"]) <= self.max_length:
                            self.person_imgs[pos].append(aligned_frame)

                        if pos == "Center" and len(self.person_imgs["Center"]) <= self.max_length:
                            self.person_imgs[pos].append(aligned_frame)

                        left = len(self.person_imgs["Left"]) != 0
                        right = len(self.person_imgs["Right"]) != 0
                        center = len(self.person_imgs["Center"]) != 0

                        if left and right and center and self.running:
                            self.allowStopIdentification.emit()
                       # elif self.running:
                           # frame[10:10+aligned_frame.shape[0], 10:10+aligned_frame.shape[1]] = aligned_frame

                        self.updateImage.emit(frame)
                    self.updateImage.emit(frame)

                if self.success:
                    frame = gaussian_filter(frame, sigma=12)

                self.updateImage.emit(frame)

            if self.success:
                print("save result")
                self.save()

        except Exception as e:
            self.disableCam.emit()
            self.interrupt()

    @pyqtSlot(name="interrupt")
    def interrupt(self):
        self.running = False
        print("Идентификация прервана по просьбе пользователя")
        print("Identification thread is killed")
        self.wait()

    @pyqtSlot(name="saveIdentificationResult")
    def stop(self):
        self.running = False
        self.success = True

    def save(self):
        print("save result")
        try:
            try:
                for pos in self.person_imgs:
                    self.person_features[pos] = [np.mean(self.extract_feature.get_features(self.person_imgs[pos]), axis=0).tolist()]
            except Exception as e:
                print("Недостаточно данных. Возможно, обучение производилось по фотографии. Попробуйте "
                              "покрутить фотографию")
                print("Неточное обучение")
            self.data_set[self.name] = self.person_features
            f = open(self.config.get_json_path(), 'w')
            f.write(json.dumps(self.data_set))
            self.successfulSaveIdentificationResult.emit(self.name)
            print("save finish")
            self.quit()
        except Exception as e:
            print(str(e))

