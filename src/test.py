from PyQt5.QtWidgets import QApplication
from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5.QtCore import pyqtSlot, pyqtSignal, Qt, QVariant
from PyQt5.QtGui import QIcon

import cv2
import os
import json
import argparse

from src.QImageItem import MyImageProvider
from src.threads.Loader import Loader

from src.threads.Identification import Identification as IdentificationThread
from src.threads.Verification import Verification as RecognitionThread
from src.Settings import Settings


class App(QQmlApplicationEngine):

    identificationSaveResult = pyqtSignal()
    interruptIdentificationSignal = pyqtSignal()
    interruptRecognitionSignal = pyqtSignal()

    re = cv2.face.LBPHFaceRecognizer_create()

    def __init__(self, *__args, args=None):
        QQmlApplicationEngine.__init__(self)
        self.args = args
        self.config = Settings(args)

        # и регистрируем его в контексте QML
        self.rootContext().setContextProperty("application", self)
        self.loader_thread = Loader()
        self.loader_thread.start()

        self.loader_thread.finishLoad.connect(self.finish_loading_modules)

        self.load("base.qml")
        self.quit.connect(app.quit)

        self.aligner = None
        self.extract_feature = None
        self.face_detect = None
        self.cam_thread = None
        self.m = None

        self.identificationThread = None
        self.recognitionThread = None

    def finish_loading_modules(self, aligner, extract_feature, face_detect):
        print("loading finish")
        (self.rootObjects()[0]).setSettingsParameters(self.config.get_json_str())
        self.aligner = aligner
        self.extract_feature = extract_feature
        self.face_detect = face_detect

        self.m = MyImageProvider()
        self.m.set_callback(self.rootObjects()[0])

        self.addImageProvider("myprovider", self.m)
        (self.rootObjects()[0]).finishLoading()

    def show_maximized(self):
        win = self.rootObjects()[0]
        win.showMaximized()

    def show(self):
        win = self.rootObjects()[0]
        win.show()

    @pyqtSlot()
    def show_minimized(self):
        win = self.rootObjects()[0]
        win.showMinimized()

    def interrupt_loading(self):
        if self.loader_thread is not None:
            self.loader_thread.interrupt()

    def hide(self):
        win = self.rootObjects()[0]
        win.hide()

    # Прерывание процесса идентификации
    def interrupt_identification(self):
        try:
            if self.identificationThread is not None:
                print("interrupt identification")
                self.interruptIdentificationSignal.emit()
                self.identificationThread = None
        except Exception as e:
            print("4: " + str(e))

    # Прерывание процесса распознавания
    def interrupt_recognition(self):
        try:
            if self.recognitionThread is not None:
                print("interrupt recognition")
                self.interruptRecognitionSignal.emit()
                self.recognitionThread = None
        except Exception as e:
            print("4: " + str(e))

    @pyqtSlot(str)
    def start_identification(self, name):
        if self.config.refresh_cam() is None:
            print("Камера не доступна")
            return

        if name == "":
            print("Имя не введено")
            return

        self.interrupt_recognition()

        if self.identificationThread is None:
            self.identificationThread = IdentificationThread(
                face_detect=self.face_detect, aligner=self.aligner, extract_feature=self.extract_feature,
                name=name, config=self.config
            )
            (self.rootObjects()[0]).startIdentification()
            self.identificationSaveResult.connect(self.identificationThread.stop)
            self.interruptIdentificationSignal.connect(self.identificationThread.interrupt)
            self.identificationThread.allowStopIdentification.connect(self.stop_identification_allow)
            self.identificationThread.successfulSaveIdentificationResult.connect(
                self.identification_save_result_callback
            )
            self.identificationThread.disableCam.connect(self.cam_is_disabled)
            self.identificationThread.start()
            self.m.set_cam_thread(self.identificationThread)

            print("Необходимо получить особенности лица. Поворачивайте голову.")
        else:
            print("Уже запущено")

    @pyqtSlot()
    def stop_identification_allow(self):
        (self.rootObjects()[0]).allowSuccessfulFinishgIdentification()

    # Успешное завершение процесса обучения (кнопка "Завершить идентификацию")
    @pyqtSlot()
    def stop_identification(self):
        try:
            print("request to stop identification")
            if self.identificationThread is not None:
                print("identification save result emit")
                self.identificationSaveResult.emit()
        except Exception as e:
            print("4: " + str(e))

    @pyqtSlot(str)
    def start_recognition(self, name):
        if name == "":
            print("Имя не введено")
            return

        if self.config.refresh_cam() is not None:
            (self.rootObjects()[0]).stopIdentification()
            self.interrupt_identification()
            self.interrupt_recognition()

            self.recognitionThread = RecognitionThread(
                face_detect=self.face_detect, aligner=self.aligner, extract_feature=self.extract_feature,
                name=name, config=self.config
            )
            self.interruptRecognitionSignal.connect(self.recognitionThread.interrupt)
            self.recognitionThread.disableCam.connect(self.cam_is_disabled)
            self.recognitionThread.start()
            self.m.set_cam_thread(self.recognitionThread)
            print("Процесс распознавания начат.")
        else:
            print("Камера не доступна")

    # Коллбек, вызываемый процессом обучения (успешное сохранение данных)
    @pyqtSlot(str)
    def identification_save_result_callback(self, name):
        if self.config.refresh_cam() is None:
            print("Камера не доступна")
            return

        self.identificationThread = None
        if self.recognitionThread is None:
            (self.rootObjects()[0]).stopIdentification()
            self.recognitionThread = RecognitionThread(
                face_detect=self.face_detect, aligner=self.aligner, extract_feature=self.extract_feature,
                name=name, config=self.config
            )
            self.interruptRecognitionSignal.connect(self.recognitionThread.interrupt)
            self.recognitionThread.disableCam.connect(self.cam_is_disabled)
            self.recognitionThread.start()
            (self.rootObjects()[0]).allowActionInFinishingIdentification()
            self.m.set_cam_thread(self.recognitionThread)
            print("Процесс распознавания начат.")
        else:
            print("Уже запущено")

    @pyqtSlot(str)
    def delete_user(self, name):
        try:
            if name == "":
                print("Задано пустое имя")
                return

            f = open(self.config.get_json_path(), 'r')
            data_set = json.loads(f.read())

            if name in data_set:
                data_set.pop(name)
                f = open(self.config.get_json_path(), 'w')
                f.write(json.dumps(data_set))
                print("Пользователь " + name + " удален")
            else:
                print("Пользователь " + name + " не найден в базе")
            f.close()
        except Exception as e:
            print("3:" + str(e))

    @pyqtSlot(str)
    def save_config(self, conf):
        self.config.update(eval(conf))
        try:
            (self.rootObjects()[0]).setSettingsParameters(self.config.get_json_str())
        except Exception as e:
            print(str(e))

    @pyqtSlot()
    def close_app(self):
        try:
            self.interrupt_recognition()
            self.interrupt_identification()
            self.interrupt_loading()
        except Exception as e:
            print(str(e))

    @pyqtSlot()
    def cam_is_disabled(self):
        self.config.vs.release()
        self.config.vs = None
        self.m.update_image(None)
        self.interrupt_recognition()
        self.interrupt_identification()


if __name__ == '__main__':
    import sys

    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

    sys.argv += ['--style', 'universal']

    parser = argparse.ArgumentParser()
    parser.add_argument("--rotation", type=str, help="Camera rotation", default="0")
    parser.add_argument("--cam", type=str, help="Camera number", default="0")
    parser.add_argument("--json", type=str, help="Path to store face ",
                        default=__location__ + "/threads/storage.json")
    parser.add_argument("--faceSizeMin", type=str, help="Size of crop face", default="80")
    parser.add_argument("--style", type=str, help="theme", default="universal")
    args = parser.parse_args(sys.argv[1:])

    app = QApplication(sys.argv)
    app.setAttribute(Qt.AA_UseOpenGLES)
    app.setWindowIcon(QIcon('spbstu.png'))
    app.setAttribute(Qt.AA_DisableHighDpiScaling)
    # создаём QML движок
    engine = App(args=args)
    engine.show()
    app.aboutToQuit.connect(lambda: engine.close_app())
    sys.exit(app.exec_())