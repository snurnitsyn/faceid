from PyQt5.QtQuick import QQuickImageProvider
from PyQt5.QtGui import QImage, QColor
from PyQt5.QtCore import Qt


class MyImageProvider(QQuickImageProvider):
    def __init__(self):
        super(MyImageProvider, self).__init__(QQuickImageProvider.Image)
        self.cam_thread = None
        self.frame = None
        self.callback = None

    def set_callback(self, callback):
        self.callback = callback

    def set_cam_thread(self, cam_thread):
        self.cam_thread = cam_thread
        self.cam_thread.updateImage.connect(self.update_image)

    def requestImage(self, p_str, size):
        if self.frame is not None:
            image = QImage(self.frame, self.frame.shape[1], self.frame.shape[0], self.frame.shape[1] * 3, \
                           QImage.Format_RGB888).rgbSwapped()
            return image, image.size()

        image = QImage(300, 300, QImage.Format_RGBA8888)
        image.fill(QColor(25, 25, 25))
        return image, image.size()

    def update_image(self, frame):
        self.frame = frame
        if self.callback is not None:
            self.callback.updateImage()
